
document.addEventListener('DOMContentLoaded', function() {
    var scrollButton = document.querySelector('#intro a.button');
    var nextSection = document.querySelector('#one');

    scrollButton.addEventListener('click', function(event) {
        event.preventDefault();
        nextSection.scrollIntoView({ behavior: 'smooth' });
    });
    var scrollButton1 = document.querySelector('#one a.button');
    var nextSection1 = document.querySelector('#two');
    
    scrollButton1.addEventListener('click', function(event) {
        event.preventDefault();
        nextSection1.scrollIntoView({ behavior: 'smooth' });
    });
    var scrollButton2 = document.querySelector('#two a.button');
    var nextSection2 = document.querySelector('#contact');
    
    scrollButton2.addEventListener('click', function(event) {
        event.preventDefault();
        nextSection2.scrollIntoView({ behavior: 'smooth' });
    });
});


document.addEventListener('DOMContentLoaded', function() {
    var scrollButton = document.querySelector('#intro-header');
    var nextSection = document.querySelector('#intro');

    scrollButton.addEventListener('click', function(event) {
        event.preventDefault();
        nextSection.scrollIntoView({ behavior: 'smooth' });
    });
    var scrollButton1 = document.querySelector('#one-header');
    var nextSection1 = document.querySelector('#one');
    
    scrollButton1.addEventListener('click', function(event) {
        event.preventDefault();
        nextSection1.scrollIntoView({ behavior: 'smooth' });
    });
    var scrollButton2 = document.querySelector('#two-header');
    var nextSection2 = document.querySelector('#two');
    
    scrollButton2.addEventListener('click', function(event) {
        event.preventDefault();
        nextSection2.scrollIntoView({ behavior: 'smooth' });
    });
    var scrollButton3 = document.querySelector('#contact-header');
    var nextSection3 = document.querySelector('#contact');
    
    scrollButton3.addEventListener('click', function(event) {
        event.preventDefault();
        nextSection3.scrollIntoView({ behavior: 'smooth' });
    });
});


  // Add hover animation for form inputs
  document.addEventListener('DOMContentLoaded', function() {
    var formInputs = document.querySelectorAll('#contact-form input, #contact-form textarea');

    formInputs.forEach(function(input) {
        input.addEventListener('mouseover', function() {
            input.style.backgroundColor = '#f9f9f9';
        });

        input.addEventListener('mouseout', function() {
            input.style.backgroundColor = 'transparent';
        });
    });
});



// Function to animate elements by fading in and sliding up
function animate(element, op) {
    setTimeout(function() {
        element.style.opacity = op;
        element.style.transform = "translateY(0)";
    }, 2000);
}

document.addEventListener('DOMContentLoaded', function() {
  var icons = document.querySelectorAll('#footer .icons .icon');

  icons.forEach(function(input) {
      input.addEventListener('mouseover', function() {
          input.style.opacity = 0.6;
      });

      input.addEventListener('mouseout', function() {
          input.style.opacity = 0.2;
      });
  });
});


document.addEventListener("DOMContentLoaded",function(){
    var one = document.getElementById("one");
    one.addEventListener("mouseout",function(){

        animate(document.getElementById("section-one"),0);
    });
    one.addEventListener("mouseover",function(){
        animate(document.getElementById("section-one"),1);
    });
})